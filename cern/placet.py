#  This file is part of PLACET
#
# Copyright (C) 2012 Yngve Inntjore Levinsen <yngve.inntjore.levinsen@cern.ch>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file COPYING.LIB.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
import subprocess
    
def placet(script,nslice,nperslice):
    '''
    Run placet with nslice slices and
    nperslice particles per slice.
    Runs in silent mode...
    '''
    subprocess.call(['placet-octave', '-s', script])

def get_array(name,atype=float):
    import placet_python as placet
    arr=placet.Tcl_GetVar(name)
    if ',' in arr: # octave 3.0 and friends..
        return [ atype(i) for i in arr.strip('[]').split(',')]
    else: # more modern octave..
        return [ atype(i) for i in arr.strip('[]').split()]

