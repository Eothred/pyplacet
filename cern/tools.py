#  This file is part of PLACET
#
# Copyright (C) 2012 Yngve Inntjore Levinsen <yngve.inntjore.levinsen@cern.ch>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Library General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Library General Public License for more details.
#
#  You should have received a copy of the GNU Library General Public License
#  along with this library; see the file COPYING.LIB.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
import subprocess

def grid(el_file='',pos_file='',stdout=None):
    '''
    Run grid in a subprocess
    '''
    cmd=['grid']
    if el_file and pos_file:
        cmd.extend([el_file,pos_file])
    result=subprocess.check_output(cmd).split()
    res_final=[[float(result[i*2+j]) for j in range(2)] for i in xrange(3)]
    return res_final

def get_offset(res):
    '''
    Give the result from grid()
    to this function, and in return
    you get the offset horizontally and vertically
    (tuple)
    '''
    offset_x =-0.5*(res[0][0]+res[0][1])
    offset_y =-0.5*(res[1][0]+res[1][1])
    return offset_x,offset_y

